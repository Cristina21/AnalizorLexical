package lab1_final;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class Ts {

    private final BinaryTree tsBinaryTree;

    public Ts() {
        tsBinaryTree = new BinaryTree();
    }

    public void writeTsToFile(File fileTs) {
        try (PrintWriter printWriter = new PrintWriter(fileTs, "UTF8")) {
            printWriter.print(tsBinaryTree.inOrderTraverseTree(tsBinaryTree.getRoot(), new StringBuilder()));
            printWriter.close();
        } catch (IOException e) {
            System.out.println("Eroare la scrierea in fisier");
            e.printStackTrace();
        }
    }

    public void addSymbol(String symbolName) {
        tsBinaryTree.addNode(symbolName);
    }

    public Integer getPosition(String symbolName) {
        return tsBinaryTree.findNode(symbolName);
    }
}

package lab1_final;

import java.io.File;

/**
 * Created by Cristina on 10/24/2016.
 */
public class Main {

    private final static File FILE_TS = new File("src\\main\\resources\\TS.txt");
    private final static File FILE_FIP = new File("src\\main\\resources\\FIP.txt");
//    private final static String FILE_CODE = "src\\main\\resources\\cmmdc.txt";
    private final static String FILE_CODE = "src\\main\\resources\\perimetruAria.txt";
//    private final static String FILE_CODE = "src\\main\\resources\\sumaN.txt";

    public static void main(String[] args) {
        Analyzer analyzer = new Analyzer();
        analyzer.analyze(FILE_CODE);
        analyzer.writeFipToFile(FILE_FIP);
        analyzer.getTs().writeTsToFile(FILE_TS);
    }
}

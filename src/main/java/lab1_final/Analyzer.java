package lab1_final;

import common.Constants;
import common.FileUtils;
import lab2.FSAConstant;
import lab2.FSAIdentifier;
import lab2.Validator;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Cristina on 10/24/2016.
 */
public class Analyzer {

    private final Map<String, Integer> codes;
    private final List<Atom> atoms;
    private final Ts ts;
    private final List<Atom> fip;

    public Analyzer() {
        codes = new HashMap<>();
        atoms = new ArrayList<>();
        initCodes();
        ts = new Ts();
        fip = new ArrayList<>();
    }

    public void analyze(String fileName) {
        readAtomsFromFile(fileName); // read file and set the code (number) for each atom
        initAtoms();
    }

    private void readAtomsFromFile(String fileName) {
        try {
            List<String> lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
            for (int lineNr = 0; lineNr < lines.size(); lineNr++) {
                String[] atomsStr = lines.get(lineNr).split(Constants.WHITE_SPACE);
                for (String atomName : atomsStr) {
                    Atom atom = new Atom(atomName.trim(), lineNr + 1);
                    atoms.add(atom);
                }
            }
        } catch (IOException e) {
            System.out.println("Error at reading file: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void initAtoms() {
        int errorCount = 0;
        for (Atom atom : atoms) {
            atom.setCode(findCode(atom.getName()));
            if (atom.getCode() == -1) {
                errorCount++;
                System.out.println("Error at: " + atom.getName() + ", at line: " + atom.getLine());
            }
            if (atom.getCode() == 0 || atom.getCode() == 1) { // daca atomul e identificator sau constanta, se adauga in ts
                ts.addSymbol(atom.getName());
            }
            atom.setPossition(ts.getPosition(atom.getName())); // pozitie potrivita, sau -1 daca nu e in ts
            fip.add(atom);
        }
        System.out.println("Done. There are " + errorCount + " errors.");
    }

    private int findCode(String atomName) {
        Integer code = codes.get(atomName);
        if (code != null) {
            return code;
        }
        if (isIdentifier(atomName)) {
            return codes.get("identifier");
        }
        if (isConstant(atomName)) {
            return codes.get("constant");
        }
        return -1; // Error code -> the atom is not a symbol
    }

    private boolean isIdentifier(String atomName) {
        if (atomName.length() > 250)
            return false;
        FSAIdentifier fsaIdentifier = new FSAIdentifier();
        return Validator.validate(fsaIdentifier, atomName);

//        String regEx = "([a-zA-Z]+[0-9a-zA-Z]*)";
//        Pattern pattern = Pattern.compile(regEx);
//        Matcher matcher = pattern.matcher(atomName);
//        return matcher.matches();
    }

    private boolean isConstant(String atomName) {
        FSAConstant fsaConstant = new FSAConstant();
        return Validator.validate(fsaConstant, atomName);

//        String regEx = "([+-]?[1-9]+[0-9]*)|([+-]?[0-9]+\\.+[0-9]+)|(0)";
//        Pattern pattern = Pattern.compile(regEx);
//        Matcher matcher = pattern.matcher(atomName);
//        return matcher.matches();
    }

    public void writeFipToFile(File fileFip) {
        try (PrintWriter printWriter = new PrintWriter(fileFip, "UTF8")) {
            for (Atom atom : fip) {
                printWriter.println(atom.getCode() + Constants.WHITE_SPACE + atom.getPossitionTs());
            }
            printWriter.close();
        } catch (IOException e) {
            System.out.println("Eroare la scrierea in fisier");
            e.printStackTrace();
        }
    }

    public Ts getTs() {
        return ts;
    }

    private void initCodes() {
        codes.put("identifier", 0);
        codes.put("constant", 1);
        codes.put("start", 2);
        codes.put("end", 3);
        codes.put("int", 4);
        codes.put("double", 5);
        codes.put("array", 6);
        codes.put("of", 7);
        codes.put("if", 8);
        codes.put("then", 9);
        codes.put("endif", 10);
        codes.put("else", 11);
        codes.put("endelse", 12);
        codes.put("while", 13);
        codes.put("do", 14);
        codes.put("endwhile", 15);
        codes.put("read", 16);
        codes.put("write", 17);
        codes.put(":", 18);
        codes.put(";", 19);
        codes.put(",", 20);
        codes.put(".", 21);
        codes.put("+", 22);
        codes.put("-", 23);
        codes.put("*", 24);
        codes.put("%", 25);
        codes.put("(", 26);
        codes.put(")", 27);
        codes.put("[", 28);
        codes.put("]", 29);
        codes.put("=", 30);
        codes.put("==", 31);
        codes.put("!=", 32);
        codes.put("<", 33);
        codes.put(">", 34);
        codes.put("<=", 35);
        codes.put(">=", 36);
    }
}

package lab1_final;

/**
 * Created by Cristina on 10/24/2016.
 */
public class Atom {

    private final String name;
    private Integer code;
    private Integer possitionTs;
    private final Integer lineNr;

    public Atom(String atomName, Integer lineNr) {
        name = atomName;
        this.lineNr = lineNr;
    }

    public String getName() {
        return name;
    }

    public Integer getCode() {
        return code;
    }

    public Integer getPossitionTs() {
        return possitionTs;
    }

    public Integer getLine() {
        return lineNr;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setPossition(Integer possitionTs) {
        this.possitionTs = possitionTs;
    }

    @Override
    public String toString() {
        return "Name: " + name + "; Code: " + code + "; PositionTs: " + possitionTs + "; LineNr: " + lineNr;
    }
}

package lab1_final;

import common.Constants;

/**
 * Created by Cristina on 10/29/2016.
 */
public class BinaryTree {

    private Node root;
    private static int key = 0;

    public void addNode(String name) {
        final Node newNode = new Node(key, name);
        key++;
        if (root == null) {
            root = newNode;
        } else {
            Node focusNode = root;
            while (true) {
                Node parent = focusNode;
                if (name.compareTo(focusNode.getName()) == 0) {
                    return;
                } else if (name.compareTo(focusNode.getName()) < 0) {
                    focusNode = focusNode.getLeftChild();
                    if (focusNode == null) {
                        parent.setLeftChild(newNode);
                        return;
                    }
                } else {
                    focusNode = focusNode.getRightChild();
                    if (focusNode == null) {
                        parent.setRightChild(newNode);
                        return;
                    }
                }
            }
        }
    }

    public int findNode(String name) {
        Node focusNode = root;
        if (focusNode == null)
            return -1;
        while (name.compareTo(focusNode.getName()) != 0) {
            if (name.compareTo(focusNode.getName()) < 0) {
                focusNode = focusNode.getLeftChild();
            } else {
                focusNode = focusNode.getRightChild();
            }
            if (focusNode == null)
                return -1;
        }
        return focusNode.getKey();
    }

    public StringBuilder inOrderTraverseTree(Node focusNode, StringBuilder allNodes) {
        if (focusNode == null) {
            return allNodes;
        }
        inOrderTraverseTree(focusNode.getLeftChild(), allNodes);
        allNodes.append(focusNode.getName()).append(Constants.WHITE_SPACE).append(focusNode.getKey()).append(Constants.NEW_LINE);
        inOrderTraverseTree(focusNode.getRightChild(), allNodes);
        return allNodes;
    }

    public void preorderTraverseTree(Node focusNode) {
        if (focusNode != null) {
            System.out.println(focusNode);
            preorderTraverseTree(focusNode.getLeftChild());
            preorderTraverseTree(focusNode.getRightChild());
        }
    }

    public void postOrderTraverseTree(Node focusNode) {
        if (focusNode != null) {
            postOrderTraverseTree(focusNode.getLeftChild());
            postOrderTraverseTree(focusNode.getRightChild());
            System.out.println(focusNode);
        }
    }

    public Node getRoot() {
        return root;
    }
}

package common;


/**
 * Created by Cristina on 11/19/2016.
 */

// the class cannot be extended
public final class Constants {

    public static final String EMPTY_STRING = "";
    public static final String WHITE_SPACE = " ";
    public static final String NEW_LINE = "\n";

    // the class cannot be instatiated
    private Constants() {
    }
}

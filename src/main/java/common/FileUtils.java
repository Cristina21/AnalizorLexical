package common;

import lab2.State;
import lab2.Transition;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Cristina on 11/7/2016.
 */
public class FileUtils {

    public static List<String[]> readTransitionsFromFile(final String fileName) {
        List<String[]> listTransitions = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] elems = line.split(Constants.WHITE_SPACE);
                listTransitions.add(elems);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
        return listTransitions;
    }

    /**
     * Read the requested line from file
     *
     * @param fileName - file name
     * @return the line splited by space
     */
    public static List<String> readLineFromFile(final String fileName) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line = bufferedReader.readLine();
            return Arrays.asList(line.split(Constants.WHITE_SPACE));
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }
}

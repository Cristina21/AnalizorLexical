package lab2;

/**
 * Automat Finit
 * Finite-state machine (FSM) or finite-state automaton (FSAConstant, plural: automata),
 * or simply a state machine,
 * is a mathematical model of computation used to design both computer programs and sequential logic circuits.
 */
public class FSAConstant extends FSA {

    public static final String SYMBOLS_FILE = "src\\main\\resources\\FSAConstant\\symbols.txt";
    public static final String STATES_FILE = "src\\main\\resources\\FSAConstant\\finalStates.txt";
    public static final String FINAL_STATES_FILE = "src\\main\\resources\\FSAConstant\\states.txt";
    public static final String TRANSITIONS_FILE = "src\\main\\resources\\FSAConstant\\transitions.txt";

    public FSAConstant() {
        super(SYMBOLS_FILE, STATES_FILE, FINAL_STATES_FILE, TRANSITIONS_FILE);
    }


}

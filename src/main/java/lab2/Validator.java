package lab2;

import common.Constants;

/**
 * Created by Cristina on 11/14/2016.
 */
public class Validator {

    public static boolean validate(FSA fsa, String sequence) {
        if (sequence == null)
            return false;
        char character = sequence.charAt(0);
        State next = getNextState(fsa, fsa.getInitialState(), character);
        if (next != null) {
            for (int index = 1; index < sequence.length(); index++) {
                character = sequence.charAt(index);
                next = getNextState(fsa, next, character);
                if (next == null) {
                    return false;
                }
            }
            if (next.isFinal()) {
                return true;
            }
        }
        return false;
    }

    public static String getValidPrefix(FSA fsa, String sequence) {
        String prefix = sequence;
        while (!prefix.equals(Constants.EMPTY_STRING)) {
            if (!validate(fsa, prefix)) {
                prefix = prefix.substring(0, prefix.length() - 1);
            } else {
                return prefix;
            }
        }
        return null;
    }

    private static State getNextState(FSA fsa, State currentState, char character) {
        for (Transition transition : fsa.getTransitions()) {
            if (transition.getStartState() == currentState && transition.getSymbol().equals(String.valueOf(character))) {
                return transition.getEndState();
            }
        }
        return null;
    }
}

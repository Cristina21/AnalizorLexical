package lab2;

import common.Constants;
import common.FileUtils;

/**
 * Created by Cristina on 11/6/2016.
 */
public class State {

    private final String elem;
    private boolean isFinal;

    public State(String elem, boolean isFinal) {
        this.elem = elem;
        this.isFinal = isFinal;
    }

    public String getElem() {
        return elem;
    }

    public void setFinal(boolean aFinal) {
        isFinal = aFinal;
    }

    public boolean isFinal() {
        return isFinal;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof State && ((State) obj).getElem().equals(this.getElem());
    }

    @Override
    public String toString() {
        return elem + Constants.WHITE_SPACE;
    }
}

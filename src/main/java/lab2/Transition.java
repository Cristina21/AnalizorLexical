package lab2;

/**
 * Created by Cristina on 11/6/2016.
 */
public class Transition {
    private State initialState;
    private String symbol;
    private State finalState;

    public Transition(State initialState, String symbol, State finalState) {
        this.initialState = initialState;
        this.symbol = symbol;
        this.finalState = finalState;
    }

    @Override
    public String toString() {
        return "Initial state: " + initialState +
                " Symbol: " + symbol +
                " Final state: " + finalState;
    }

    public State getStartState() {
        return initialState;
    }

    public String getSymbol() {
        return symbol;
    }

    public State getEndState() {
        return finalState;
    }
}

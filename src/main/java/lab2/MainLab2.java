package lab2;

import common.Constants;

import java.util.Scanner;

/**
 * Created by Cristina on 11/6/2016.
 */
public class MainLab2 {

    private final static Scanner scanner = new Scanner(System.in);
    private static FSAConstant fsaConstant;
    private static FSAIdentifier fsaIdentifier;

    public static void main(String[] args) {
        fsaConstant = new FSAConstant();
        fsaIdentifier = new FSAIdentifier();
        try {
            while (true) {
                int cmd = 0;
                while (cmd != 7) {
                    printMenu();
                    cmd = readCmd();
                    switch (cmd) {
                        case 1:
                            dispaySetOfStates(fsaConstant);
                            dispaySetOfStates(fsaIdentifier);
                            break;
                        case 2:
                            displayAlphabet(fsaConstant);
                            displayAlphabet(fsaIdentifier);
                            break;
                        case 3:
                            dispayTransitions(fsaConstant);
                            dispayTransitions(fsaIdentifier);
                            break;
                        case 4:
                            dispaySetOfFinalStates(fsaConstant);
                            dispaySetOfFinalStates(fsaIdentifier);
                            break;
                        case 5:
                            verifySequece(fsaConstant);
                            break;
                        case 6:
                            verifySequece(fsaIdentifier);
                            break;
                        case 7:
                            break;
                        default:
                            System.out.println("Wrong command");
                            break;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void verifySequece(FSA fsa) {
        String sequence = readElement();
        boolean isValid = Validator.validate(fsa, sequence);
        if (isValid) {
            System.out.println("The sequence '" + sequence + "' is accepted by the Finit State Automata");
        } else {
            System.out.println("The sequence '" + sequence + "' is not accepted by the Finit State Automata");
            sequence = sequence.substring(0, sequence.length() - 1); //is not accepted
            String prefix = Validator.getValidPrefix(fsa, sequence);
            System.out.println("The valid prefix is: " + prefix);
        }
    }

    private static int readCmd() {
        return scanner.nextInt();
    }

    private static String readElement() {
        System.out.println("Write your element: ");
        return scanner.next();
    }

    private static void printMenu() {
        System.out.println("\nChoose one option to display: ");
        System.out.println("1. Set of states -- Q");
        System.out.println("2. Alphabet -- S");
        System.out.println("3. Transitions -- d: Q x S -> P(Q)");
        System.out.println("4. Set of final states -- F");
        System.out.println("5. Validate constant");
        System.out.println("6. Validate identifier");
        System.out.println("7. Exit");
    }

    private static void dispaySetOfStates(FSA fsa) {
        System.out.print("\nStates for: " + fsa.getClass() + ": ");
        for (State state : fsa.getStates()) {
            System.out.print(state + Constants.WHITE_SPACE);
        }
    }

    private static void displayAlphabet(FSA fsa) {
        System.out.print("\nAlphabet for: " + fsa.getClass() + ": ");
        for (String elem : fsa.getAlphabet()) {
            System.out.print(elem + Constants.WHITE_SPACE);
        }
    }

    private static void dispayTransitions(FSA fsa) {
        System.out.print("\nTransitions for: " + fsa.getClass() + ": ");
        for (Transition transition : fsa.getTransitions()) {
            System.out.println(transition + Constants.WHITE_SPACE);
        }
    }

    private static void dispaySetOfFinalStates(FSA fsa) {
        System.out.print("\nFinal states for: " + fsa.getClass() + ": ");
        for (State finalState : fsa.getFinalStates()) {
            System.out.print(finalState + Constants.WHITE_SPACE);
        }
    }
}

package lab2;

import java.util.List;

import static common.FileUtils.readLineFromFile;
import static common.FileUtils.readTransitionsFromFile;

/**
 * Automat Finit
 * Finite-state machine (FSM) or finite-state automaton (FSAIdentifier, plural: automata),
 * or simply a state machine,
 * is a mathematical model of computation used to design both computer programs and sequential logic circuits.
 */

public class FSAIdentifier extends FSA {

    public static final String SYMBOLS_FILE = "src\\main\\resources\\FSAIdentifier\\symbolsIdentifier.txt";
    public static final String STATES_FILE = "src\\main\\resources\\FSAIdentifier\\finalStatesIdentifier.txt";
    public static final String FINAL_STATES_FILE = "src\\main\\resources\\FSAIdentifier\\statesIdentifier.txt";
    public static final String TRANSITIONS_FILE = "src\\main\\resources\\FSAIdentifier\\transitionsIdentifier.txt";

    public FSAIdentifier() {
        super(SYMBOLS_FILE, STATES_FILE, FINAL_STATES_FILE, TRANSITIONS_FILE);
    }

}

package lab2;

import common.FileUtils;

import java.util.ArrayList;
import java.util.List;

import static common.FileUtils.readLineFromFile;

/**
 * Created by Cristina on 11/14/2016.
 */
public abstract class FSA {

    public static final int START_STATE = 0;
    public static final int END_STATE = 2;

    protected List<State> states;
    protected List<State> finalStates;
    protected State initialState;
    protected List<String> alphabet;
    protected List<Transition> transitions;

    FSA(final String symbolFileName, final String finalStatesFileName, final String statesFileName, final String transitionsFileName) {
        this.alphabet = getAlphabet(symbolFileName);
        this.finalStates = getFinalStates(finalStatesFileName);
        this.states = getStates(statesFileName);
        this.initialState = createInitialState();
        this.transitions = getTransitions(transitionsFileName);
    }

    protected State getByName(String elem) {
        for (State state : this.getStates()) {
            if (state.getElem().equals(elem)) {
                return state;
            }
        }
        return null;
    }

    protected List<State> getFinalStates(String fileName) {
        List<String> elements = FileUtils.readLineFromFile(fileName);
        List<State> finalStates = new ArrayList<>();
        for (String elem : elements) {
            State state = new State(elem, true);
            finalStates.add(state);
        }
        return finalStates;
    }

    protected List<State> getStates(String fileName) {
        List<String> elements = FileUtils.readLineFromFile(fileName);
        List<State> listStates = new ArrayList<>();
        for (String elem : elements) {
            State state = new State(elem, false);
            if (isFinalState(state)) {
                state.setFinal(true);
            }
            listStates.add(state);
        }
        return listStates;
    }

    protected List<Transition> getTransitions(String fileName) {
        List<String[]> elements = FileUtils.readTransitionsFromFile(fileName);
        List<Transition> listTransitions = new ArrayList<>();
        for (String elem[] : elements) {
            State startState = getByName(elem[START_STATE]);
            State endState = getByName(elem[END_STATE]);
            Transition transition = new Transition(startState, elem[1], endState);
            listTransitions.add(transition);
        }
        return listTransitions;
    }

    protected State createInitialState() {
        return states.get(0);
    }

    private boolean isFinalState(State state) {
        return finalStates != null && finalStates.contains(state);
    }

    public List<State> getStates() {
        return states;
    }

    public List<State> getFinalStates() {
        return finalStates;
    }

    public List<String> getAlphabet() {
        return alphabet;
    }

    public List<Transition> getTransitions() {
        return transitions;
    }

    public State getInitialState() {
        return initialState;
    }

    protected List<String> getAlphabet(String fileName) {
        return readLineFromFile(fileName);
    }
}

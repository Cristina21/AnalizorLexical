Laborator 1 (LFTC)

Tema: lab1_sapt4.Analizor lexical
Timp de lucru: 3 saptamani (  3 seturi de 2 ore de laborator)
Termen de predare: saptamana 5
Enunt:
Scrierea unui ANALIZOR LEXICAL pentru un minilimbaj de programare (MLP).

1. se cer textele sursa a 3 programe
(versiune electronica)
care respecta specificatiile MLP date si care rezolva urmatoarele probleme:
- calculeaza perimetrul si aria cercului de o raza data data
- determina cmmdc a 2 nr naturale
- calculeaza suma a n numere reale citite de la tastatura

2. Specificarea minilimbajului de programare.
Limbajul trebuie sa contina anumite instructiuni si tipuri de date:
- 2 tipuri de date simple si un tip compus
- instructiuni:
- o instructiune de atribuire
- o instructiune de intrare/iesire
- o instructiune de selectie (conditionala)
- o instructiune de ciclare
Se cere ca specificarea sa fie suficient de generala astfel incat sa descrie constructiile limbajului
folosite pentru scrierea programelor de la pct.1

3. Implementarea analizorului lexical
Analizorul lexical accepta la intrare un fisier text reprezentand
un program sursa si intocmeste ca date de iesire tabelele:
    FIP - forma interna a programului sursa si
    TS - tabelui de simboluri.
In plus, programul va trebui sa semnaleze erorile lexicale si locul in care apar.
Analizoarele lexicale se vor diferentia dupa urmatoarele criterii: (bab)
1. identificatori
// a. de lungime cel mult opt caractere
b. de lungime oarecare nedepasind 250 caractere
2. tabela de simboluri:
a. unica pentru identificatori si constante
// b. separat pentru identificatori si constante
3. organizarea tabelelor de simboluri:
// a. tabel ordonat lexicografic
b. tabel arbore binar lexicografic
// c. tabel de dispersie (hash)

Programul care efectueaza analiza lexicala trebuie sa fie insotit de documentatie.